<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Models\Province;
use App\Http\Requests\StoreProvinceRequest;
use App\Http\Requests\UpdateProvinceRequest;

class ProvinceController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch() {
        //
        $response = Http::withHeaders(['key'=>'0df6d5bf733214af6c6644eb8717c92c'])
                        ->get('https://api.rajaongkir.com/starter/province/');
        //return $response['rajaongkir']['results'];

        $res = 0;
        foreach($response['rajaongkir']['results'] as $rrr) {
            $data = array("province_id"=>$rrr['province_id'], "province"=>$rrr['province']);
            Province::create($data);
            $res++;
        }
        if($res == 0) return response()->json(['msg' => 'Fetching Province Fail!', 'success' => 0], $response->getStatusCode());
        return response()->json(['msg' => 'Fetching Province Success!', 'success' => 1], $response->getStatusCode());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProvinceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProvinceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province, $id) {
        //
        $data = Province::where('province_id', '=', $id)->get();
        return response()->json(['msg' => 'Search Province', 'success' => 1, 'data'=>$data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProvinceRequest  $request
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProvinceRequest $request, Province $province)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        //
    }
}
