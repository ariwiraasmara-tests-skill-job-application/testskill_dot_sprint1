<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\Models\City;
use App\Http\Requests\StoreCityRequest;
use App\Http\Requests\UpdateCityRequest;

class CityController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fetch() {
        //
        $response = Http::withHeaders(['key'=>'0df6d5bf733214af6c6644eb8717c92c'])
                        ->get('https://api.rajaongkir.com/starter/city/');
        //return $response['rajaongkir']['results'];

        $res = 0;
        foreach($response['rajaongkir']['results'] as $rrr) {
            $data = array("city_id"     => $rrr['city_id'],
                          "province_id" => $rrr['province_id'],
                          "province"    => $rrr['province'],
                          "type"        => $rrr['type'],
                          "city_name"   => $rrr['city_name'],
                          "postal_code" => $rrr['postal_code'],);
            City::create($data);
            $res++;
        }
        if($res == 0) return response()->json(['msg' => 'Fetching City Fail!', 'success' => 0], $response->getStatusCode());
        return response()->json(['msg' => 'Fetching City Success!', 'success' => 1], $response->getStatusCode());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCityRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCityRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city, $id) {
        //
        $data = City::where('city_id', '=', $id)->get();
        return response()->json(['msg' => 'Search City', 'success' => 1, 'data'=>$data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCityRequest  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCityRequest $request, City $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }
}
