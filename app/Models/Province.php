<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'province';
    //public $table = 'my101_user_login';
    protected $fillable = ["province_id",
                            "province",];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
