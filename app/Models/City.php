<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'city';
    //public $table = 'my101_user_login';
    protected $fillable = ["city_id",
                            "province_id",
                            "province",
                            "type",
                            "city_name",
                            "postal_code",];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';
}
