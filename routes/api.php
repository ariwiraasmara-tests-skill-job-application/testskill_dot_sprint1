<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/fetch/province', '\App\Http\Controllers\ProvinceController@fetch');
Route::get('/fetch/city', '\App\Http\Controllers\CityController@fetch');

Route::get('/search/provinces/{id}', '\App\Http\Controllers\ProvinceController@show');
Route::get('/search/cities/{id}', '\App\Http\Controllers\CityController@show');
