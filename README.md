# testskill_dot_sprint1

# Instalasi database
1. Buat database bernama testskill
2. Ketikkan perintah berikut untuk migrate dan menginstall tabel-tabel pada database mysql
-> php artisan migrate

# Gunakan Postman
# Instalasi dan Fetching Data Rajaongkir
1. Akses url localhost/testskill_dot_sprint1/public/api/fetch/province untuk fetching data province
2. Akses url localhost/testskill_dot_sprint1/public/api/fetch/province untuk fetching data province

# Searching Provinces dan Cities
1. Akses url localhost/testskill_dot_sprint1/public/api/search/provinces/{id} untuk mendapatkan data province tertentu berdasarkan id
2. Akses url localhost/testskill_dot_sprint1/public/api/search/cities/{id} untuk mendapatkan data cities tertentu berdasarkan id

Sebagai contoh:
1. localhost/testskill_dot_sprint1/public/api/search/provinces/2
2. localhost/testskill_dot_sprint1/public/api/search/cities/2

